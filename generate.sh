#!/usr/bin/env bash
get_services() {
    docker service ls --format '{{.Name}}' --filter label=lb.enabled=true
}

get_hostname() {
    local service="$1"
    local segment="$2"
    docker service inspect --format='{{json .Spec.Labels}}' "$service" \
        | jq '.["lb.'"${segment}"'.hostname"]' -r
}

get_port() {
    local service="$1"
    local segment="$2"
    docker service inspect --format='{{json .Spec.Labels}}' "$service" \
        | jq -re '.["lb.'"${segment}"'.port"] // empty' \
        || echo "80"
}

get_protocol() {
    local service="$1"
    local segment="$2"
    docker service inspect --format='{{json .Spec.Labels}}' "$service" \
        | jq -re '.["lb.'"${segment}"'.protocol"] // empty' \
        || echo "http"
}

get_location() {
    local service="$1"
    local segment="$2"
    docker service inspect --format='{{json .Spec.Labels}}' "$service" \
        | jq -re '.["lb.'"${segment}"'.location"] // empty' \
        || echo "/"
}

get_segments() {
    local service="$1"
    docker service inspect --format='{{json .Spec.Labels}}' "$service" \
        | jq -r '' \
        | grep -e "lb\..*\.hostname" \
        | sed -e 's/^.*"lb\.//' -e 's/\..*$//'
}

server_config() {
    local hostname="$1"

    #TODO: implement https

cat <<EOF
server {
  server_name $hostname;
  listen 80;
  gzip on;

  include /etc/nginx/conf.d/${hostname}/*.conf;
}
EOF
}

location_config() {
    local service="$1"
    local segment="$2"
    local protocol port location;

    protocol=$(get_protocol "$service" "$segment")
    port=$(get_port "$service" "$segment")
    location=$(get_location "$service" "$segment")

    #TODO: add the other X-Forwarded-* headers
    #TODO: get actual public ip, if possible

cat <<EOF
location ${location} {
  proxy_pass ${protocol}://${service}:${port};
  proxy_set_header X-Real-IP \$remote_addr;
  proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
}
EOF

}

# clean up, since we're regenerating all of it anyways
rm -rf /etc/nginx/conf.d/*

echo "updating configuration"
# loop through services
for service in $(get_services); do
    # loop through services
    for segment in $(get_segments "$service"); do

        _hostname=$(get_hostname "$service" "$segment")

        # create server block
        mkdir -p "/etc/nginx/conf.d/${_hostname}"
        server_config "$_hostname" > "/etc/nginx/conf.d/${_hostname}.conf"

        # create location block
        location_config "$service" "$segment" > "/etc/nginx/conf.d/${_hostname}/${service}_${segment}.conf"
    done
done
nginx -s reload
