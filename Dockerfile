FROM nginx
WORKDIR /app/
STOPSIGNAL SIGQUIT

RUN apt-get update && apt-get install -y curl jq procps
RUN curl -s https://download.docker.com/linux/static/stable/x86_64/docker-19.03.5.tgz \
    | tar zxf - -C /usr/local/bin/ --strip-components=1

ENTRYPOINT ["/app/entrypoint.sh"]

COPY . .
