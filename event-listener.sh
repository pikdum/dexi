#!/usr/bin/env bash

#TODO: make sure this triggers for service removal too
docker events --since 0m --filter scope=swarm --filter type=service --filter event=update \
    | xargs -I % ./generate.sh
