#!/usr/bin/env bash

./generate.sh
./event-listener.sh &
exec nginx -g 'daemon off;'
